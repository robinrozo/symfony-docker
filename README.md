Infrastructure docker pour projet Symfony avec:
* Nginx
* Php 7 FPM
* Mysql 5.7
* Mailhog

## Commandes
* lancer les containers: `docker-compose -f docker-compose-dev.yml up -d`
* arrêter les containers: `docker-compose -f docker-compose-dev.yml stop`
* accéder à un container: `docker exec -it <nom_container> sh`

## Dossiers
* **containers**: dossier des fichiers de conf et de Dockerfile
* **logs**: logs des containers
* **data**: données des containers
* **www**: code de l'application

## Container PHP
Les commandes `symfony` et `composer` sont disponibles.
